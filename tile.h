#ifndef _TILE_H
#define _TILE_H

typedef struct GAME_OBJECT_TILE {

    int fallStepTime;
    int moveStepTime;

} GAME_OBJECT_TILE;

GAME_OBJECT *TileConstructor(GAME_STATE *state, int isActive);
void TileDestructor(GAME_STATE *state, GAME_OBJECT *self);
void TileStep(GAME_STATE *state, GAME_OBJECT *self);
void TileRender(GAME_STATE *state, GAME_OBJECT *self);

#endif // _TILE_H
