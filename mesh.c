//#ifndef _BLOCK_C
//#define _BLOCK_C

#include "GL/gl.h"

#include "ui.h"
#include "game.h"
#include "mesh.h"

int CreateCubeMesh(GAME_STATE *state) {
    if (state->numberOfMeshes < GAME_MAX_MESHES) {
        int id = 0;//state->numberOfMeshes;
        int nextId = state->numberOfMeshes++;
        printf("makecubemesh id = %i\n", id);

        state->mesh[id] = glGenLists(1);
        printf("state->mesh[state->numberOfMeshes] = %i\n", state->mesh[0]);

        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();

        glNewList(state->mesh[id], GL_COMPILE);

        //glPushMatrix();
        glBegin(GL_QUADS);

        // front face GOOD
        glColor3f(0.0f, 0.0f, 1.0f);
        glVertex3f(-1.0f, 1.0f, -1.0f);

        glColor3f(0.0f, 0.0f, 1.0f);
        glVertex3f(1.0f, 1.0f, -1.0f);

        glColor3f(0.0f, 0.0f, 1.0f);
        glVertex3f(1.0f, -1.0f, -1.0f);

        glColor3f(0.0f, 0.0f, 1.0f);
        glVertex3f(-1.0f, -1.0f, -1.0f);

        // back face
        glColor3f(1.0f, 0.0f, 0.0f);
        glVertex3f(1.0f, 1.0f, 1.0f);

        glColor3f(1.0f, 0.0f, 0.0f);
        glVertex3f(-1.0f, 1.0f, 1.0f);

        glColor3f(1.0f, 0.0f, 0.0f);
        glVertex3f(-1.0f, -1.0f, 1.0f);

        glColor3f(1.0f, 0.0f, 0.0f);
        glVertex3f(1.0f, -1.0f, 1.0f);


        // left face
        glColor3f(0.0f, 1.0f, 1.0f);
        glVertex3f(-1.0f, 1.0f, 1.0f);

        glColor3f(0.0f, 1.0f, 1.0f);
        glVertex3f(-1.0f, 1.0f, -1.0f);

        glColor3f(0.0f, 1.0f, 1.0f);
        glVertex3f(-1.0f, -1.0f, -1.0f);

        glColor3f(0.0f, 1.0f, 1.0f);
        glVertex3f(-1.0f, -1.0f, 1.0f);

        // right face
        glColor3f(1.0f, 0.0f, 1.0f);
        glVertex3f(1.0f, 1.0f, -1.0f);

        glColor3f(1.0f, 0.0f, 1.0f);
        glVertex3f(1.0f, 1.0f, 1.0f);

        glColor3f(1.0f, 0.0f, 1.0f);
        glVertex3f(1.0f, -1.0f, 1.0f);

        glColor3f(1.0f, 0.0f, 1.0f);
        glVertex3f(1.0f, -1.0f, -1.0f);

        // top face
        glColor3f(1.0f, 1.0f, 0.0f);
        glVertex3f(1.0f, 1.0f, 1.0f);

        glColor3f(1.0f, 1.0f, 0.0f);
        glVertex3f(1.0f, 1.0f, -1.0f);

        glColor3f(1.0f, 1.0f, 0.0f);
        glVertex3f(-1.0f, 1.0f, -1.0f);

        glColor3f(1.0f, 1.0f, 0.0f);
        glVertex3f(-1.0f, 1.0f, 1.0f);

        // bottom face
        glColor3f(1.0f, 1.0f, 1.0f);
        glVertex3f(1.0f, -1.0f, 1.0f);

        glColor3f(1.0f, 1.0f, 1.0f);
        glVertex3f(-1.0f, -1.0f, 1.0f);

        glColor3f(1.0f, 1.0f, 1.0f);
        glVertex3f(-1.0f, -1.0f, -1.0f);

        glColor3f(1.0f, 1.0f, 1.0f);
        glVertex3f(1.0f, -1.0f, -1.0f);

        glEnd();
        //glPopMatrix();

        glEndList();

        glTranslatef(0.0f, 0.0f, -2.5f);
        glRotatef(0.0f, 1.0f, 1.0f, 0.0f);
        glScalef(0.25f, 0.25f, 0.25f);

        return id;
    }
    return -1;
}

//#endif // _BLOCK_C
