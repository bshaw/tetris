
#include "GL/gl.h"

#include "ui.h"
#include "game.h"
#include "iblock.h"
#include "tile.h"

void IBlockConstructor(GAME_STATE *state) {

    int x1 = (GAME_FIELD_WIDTH/2) - 2;
    int y1 = 0;
    GAME_OBJECT *tile1 = TileConstructor(state, 1);
    tile1->x = state->field[x1][y1].x;
    tile1->y = state->field[x1][y1].y;
    state->field[x1][y1].object = tile1;

    int x2 = (GAME_FIELD_WIDTH/2) - 1;
    int y2 = 0;
    GAME_OBJECT *tile2 = TileConstructor(state, 1);
    tile2->x = state->field[x2][y2].x;
    tile2->y = state->field[x2][y2].y;
    state->field[x2][y2].object = tile2;

    int x3 = (GAME_FIELD_WIDTH/2);
    int y3 = 0;
    GAME_OBJECT *tile3 = TileConstructor(state, 1);
    tile3->x = state->field[x3][y3].x;
    tile3->y = state->field[x3][y3].y;
    state->field[x3][y3].object = tile3;

    int x4 = (GAME_FIELD_WIDTH/2) + 1;
    int y4 = 0;
    GAME_OBJECT *tile4 = TileConstructor(state, 1);
    tile4->x = state->field[x4][y4].x;
    tile4->y = state->field[x4][y4].y;
    state->field[x4][y4].object = tile4;

    GameAddObject(state, tile1);
    GameAddObject(state, tile2);
    GameAddObject(state, tile3);
    GameAddObject(state, tile4);

    state->activeTiles[0].inUse = 1;
    state->activeTiles[0].x = x1;
    state->activeTiles[0].y = y1;

    // tile1;
    state->activeTiles[1].inUse = 1;
    state->activeTiles[1].x = x2;
    state->activeTiles[1].y = y2;

    state->activeTiles[2].inUse = 1;
    state->activeTiles[2].x = x3;
    state->activeTiles[2].y = y3;

    state->activeTiles[3].inUse = 1;
    state->activeTiles[3].x = x4;
    state->activeTiles[3].y = y4;

    state->activeTilesRect.x = tile1->x;
    state->activeTilesRect.y = tile1->y;
    state->activeTilesRect.h = GAME_FIELD_SQUARE_SIZE;
    state->activeTilesRect.w = GAME_FIELD_SQUARE_SIZE * 4;

    state->activeTileType = GAME_TILE_TYPE_I;

    state->activeTileOrientation = 0;

    printf("0: ax = %i, ay = %i\n", state->activeTiles[0].x, state->activeTiles[0].y);
    printf("1: ax = %i, ay = %i\n", state->activeTiles[1].x, state->activeTiles[1].y);
    printf("2: ax = %i, ay = %i\n", state->activeTiles[2].x, state->activeTiles[2].y);
    printf("3: ax = %i, ay = %i\n", state->activeTiles[3].x, state->activeTiles[3].y);

    printf("0: wx = %lf, wy = %lf\n", state->field[x1][y1].x, state->field[x1][y1].y);
    printf("1: wx = %lf, wy = %lf\n", state->field[x2][y2].x, state->field[x2][y2].y);
    printf("2: wx = %lf, wy = %lf\n", state->field[x3][y3].x, state->field[x3][y3].y);
    printf("3: wx = %lf, wy = %lf\n", state->field[x4][y4].x, state->field[x4][y4].y);


}
void IBlockRotate(GAME_STATE *state) {
    if (state->activeTileOrientation == 0) {
        IBlockRotateA(state);
    }
    else  if (state->activeTileOrientation == 1) {
        IBlockRotateB(state);
    }
}


void IBlockRotateA(GAME_STATE *state) {

    unsigned int ax1 = state->activeTiles[0].x;
    unsigned int ay1 = state->activeTiles[0].y;

    unsigned int ax2 = state->activeTiles[1].x;
    unsigned int ay2 = state->activeTiles[1].y;

    unsigned int ax3 = state->activeTiles[2].x;
    unsigned int ay3 = state->activeTiles[2].y;

    unsigned int ax4 = state->activeTiles[3].x;
    unsigned int ay4 = state->activeTiles[3].y;

    GAME_OBJECT *activeTile1 = state->field[ax1][ay1].object;
    GAME_OBJECT *activeTile2 = state->field[ax2][ay2].object;
    GAME_OBJECT *activeTile3 = state->field[ax3][ay3].object;
    GAME_OBJECT *activeTile4 = state->field[ax4][ay4].object;

    state->field[ax1][ay1].object = NULL;
    state->field[ax2][ay2].object = NULL;
    state->field[ax3][ay3].object = NULL;
    state->field[ax4][ay4].object = NULL;

    if ((ax1 > 0 && ax2 > 0 && ax3 > 0 && ax4 > 0) &&
        (ax1 < GAME_FIELD_WIDTH &&
         ax2 < GAME_FIELD_WIDTH &&
         ax3 < GAME_FIELD_WIDTH &&
         ax4 < GAME_FIELD_WIDTH) &&
        (ay1 - 1 > 1 && ay2 > 1 && ay3 + 1 > 1 && ay4 + 2 > 1) && (ay1 - 1 > 1 && ay2 > 1 && ay3 + 1 > 1 && ay4  +2 > 1) &&
        (ay1 - 1 < GAME_FIELD_HEIGHT - 1 && ay2 < GAME_FIELD_HEIGHT - 1 && ay3 + 1 < GAME_FIELD_HEIGHT - 1 && ay4 + 2 < GAME_FIELD_HEIGHT - 1) &&
        (   GameIsCellEmpty(state, ax2, ay1 - 1) &&
            GameIsCellEmpty(state, ax2, ay2) &&
            GameIsCellEmpty(state, ax2, ay3 + 1) &&
            GameIsCellEmpty(state, ax2, ay4 + 2))) {

        state->activeTileOrientation = 1;

        GameMoveActivePiecesToCell(state, activeTile1, ax2, ay1 - 1, 0);
        GameMoveActivePiecesToCell(state, activeTile2, ax2, ay2, 1);
        GameMoveActivePiecesToCell(state, activeTile3, ax2, ay3 + 1, 2);
        GameMoveActivePiecesToCell(state, activeTile4, ax2, ay4 + 2, 3);

    }
    else {

        state->field[ax1][ay1].object = activeTile1;
        state->field[ax2][ay2].object = activeTile2;
        state->field[ax3][ay3].object = activeTile3;
        state->field[ax4][ay4].object = activeTile4;

    }



}

void IBlockRotateB(GAME_STATE *state) {

    unsigned int ax1 = state->activeTiles[0].x;
    unsigned int ay1 = state->activeTiles[0].y;

    unsigned int ax2 = state->activeTiles[1].x;
    unsigned int ay2 = state->activeTiles[1].y;

    unsigned int ax3 = state->activeTiles[2].x;
    unsigned int ay3 = state->activeTiles[2].y;

    unsigned int ax4 = state->activeTiles[3].x;
    unsigned int ay4 = state->activeTiles[3].y;

    GAME_OBJECT *activeTile1 = state->field[ax1][ay1].object;
    GAME_OBJECT *activeTile2 = state->field[ax2][ay2].object;
    GAME_OBJECT *activeTile3 = state->field[ax3][ay3].object;
    GAME_OBJECT *activeTile4 = state->field[ax4][ay4].object;

    state->field[ax1][ay1].object = NULL;
    state->field[ax2][ay2].object = NULL;
    state->field[ax3][ay3].object = NULL;
    state->field[ax4][ay4].object = NULL;

    if ((ax1 - 1 > 0 && ax2 > 0 && ax3 + 1 > 0 && ax4 + 2 > 0) &&
        (ax1 - 1 < GAME_FIELD_WIDTH &&
         ax2 < GAME_FIELD_WIDTH &&
         ax3 + 1 < GAME_FIELD_WIDTH &&
         ax4 + 2 < GAME_FIELD_WIDTH) &&
        (ay1 > 1 && ay2 > 1 && ay3 > 1 && ay4 > 1) && (ay1 > 1 && ay2 > 1 && ay3 > 1 && ay4 > 1) &&
        (ay1 < GAME_FIELD_HEIGHT - 1 && ay2 < GAME_FIELD_HEIGHT - 1 && ay3 < GAME_FIELD_HEIGHT - 1 && ay4 < GAME_FIELD_HEIGHT - 1) &&
        (   GameIsCellEmpty(state, ax1 - 1, ay2) &&
            GameIsCellEmpty(state, ax2,     ay2) &&
            GameIsCellEmpty(state, ax3 + 1, ay2) &&
            GameIsCellEmpty(state, ax4 + 2, ay2))) {

        state->activeTileOrientation = 0;

        GameMoveActivePiecesToCell(state, activeTile1, ax1 - 1, ay2, 0);
        GameMoveActivePiecesToCell(state, activeTile2, ax2, ay2, 1);
        GameMoveActivePiecesToCell(state, activeTile3, ax3 + 1, ay2, 2);
        GameMoveActivePiecesToCell(state, activeTile4, ax4 + 2, ay2, 3);

        /*state->field[ax1 - 1][ay1].object = activeTile1;
        state->field[ax2][ay2].object = activeTile2;
        state->field[ax3 + 1][ay3].object = activeTile3;
        state->field[ax4 + 2][ay4].object = activeTile4;*/

        /*state->activeTiles[0].x = ay1;
        state->activeTiles[0].y = ax1;

        state->activeTiles[1].x = ay2;
        state->activeTiles[1].y = ax2;

        state->activeTiles[2].x = ay3;
        state->activeTiles[2].y = ax3;

        state->activeTiles[3].x = ay4;
        state->activeTiles[3].y = ax4;*/
    }
    else {

        state->field[ax1][ay1].object = activeTile1;
        state->field[ax2][ay2].object = activeTile2;
        state->field[ax3][ay3].object = activeTile3;
        state->field[ax4][ay4].object = activeTile4;

    }



}

void IBlockDestructor(GAME_STATE *state, GAME_OBJECT *self) {
    printf("henlo from TileDestructor!\n");
    //free(self->extends);
}
