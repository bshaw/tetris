#ifndef _GAME_H
#define _GAME_H

#define GAME_FIELD_HEIGHT 16
#define GAME_FIELD_WIDTH 14
#define GAME_MAX_OBJECTS 1000
#define GAME_MAX_MESHES 2
#define GAME_MAX_ACTIVE_TILES 4

#define FIELD_LEFT      -2.94f
#define FIELD_RIGHT     2.94f
#define FIELD_TOP       3.36f//2.94f
#define FIELD_BOTTOM    -2.94f
#define GAME_FIELD_SQUARE_SIZE 0.42f

#define PI 3.141592653589793

#define VK_A 0x41
#define VK_D 0x44
#define VK_E 0x45
#define VK_Q 0x51
#define VK_S 0x53
#define VK_W 0x57

enum GAME_DIRECTION {
    GAME_DIRECTION_DOWN = 0,
    GAME_DIRECTION_UP = 1,
    GAME_DIRECTION_LEFT = 2,
    GAME_DIRECTION_RIGHT = 3
};

enum GAME_TILE_TYPE {
    GAME_TILE_TYPE_I = 0,
    GAME_TILE_TYPE_O,
    GAME_TILE_TYPE_T,
    GAME_TILE_TYPE_S,
    GAME_TILE_TYPE_Z,
    GAME_TILE_TYPE_J,
    GAME_TILE_TYPE_L
};


enum GAME_MESH_ID {
    GAME_MESH_CUBE = 0,
    GAME_MESH_
};

typedef struct GAME_OBJECT {

    int uuid;
    int removed;
    int isActive;

    float x;
    float y;
    float z;

    float rotation;
    float xrotation;
    float yrotation;
    float zrotation;

    int meshId;

    void *fnConstructor;
    void *fnDestructor;
    void *fnStep;
    void *fnRender;
    void *extends;

} GAME_OBJECT;

typedef struct GAME_STATE_VIEW {

    double aspect;
    double fov;

    double left;
    double right;
    double top;
    double bottom;

    double width;
    double height;

    double znear;
    double zfar;

} GAME_STATE_VIEW;

typedef struct GAME_RECT {
    double x;
    double y;
    double w;
    double h;
} GAME_RECT;



typedef struct GAME_FIELD {
    double x;
    double y;
    GAME_OBJECT *object;
} GAME_FIELD;

typedef struct GAME_ACTIVE_TILE {
    int inUse;
    unsigned int x;
    unsigned int y;
} GAME_ACTIVE_TILE;

typedef struct GAME_STATE {

    int active;
    double dt;
    double fallSpeed;
    int frameTime;
    int fps;
    int frameCount;
    int timeStep;
    int numberOfObjects;
    int nextUUID;
    int score;
    int level;
    int activeReady;

    int mesh[GAME_MAX_MESHES];
    int numberOfMeshes;

    UIH_STATE *window;

    GAME_STATE_VIEW view;

    GAME_ACTIVE_TILE activeTiles[GAME_MAX_ACTIVE_TILES];
    GAME_RECT activeTilesRect;
    int activeTileOrientation;
    int activeTileType;
    double lastFallTime;

    GAME_OBJECT *objects[GAME_MAX_OBJECTS];
    GAME_OBJECT *objectsQueue[GAME_MAX_OBJECTS];
    GAME_FIELD field[GAME_FIELD_WIDTH][GAME_FIELD_HEIGHT];

} GAME_STATE;


typedef void (*ObjectConstructorFunc) (GAME_STATE*);
typedef void (*ObjectDestructorFunc) (GAME_STATE*, GAME_OBJECT*);
typedef void (*ObjectStepFunc) (GAME_STATE*, GAME_OBJECT*);
typedef void (*ObjectRenderFunc) (GAME_STATE*, GAME_OBJECT*);

GAME_STATE *MakeGameState(UIH_STATE* window);
void GameRender(UIH_STATE *windowState, GAME_STATE *state);
void GameFlushQueue(GAME_STATE *state);
void GameUpdate(UIH_STATE *window, GAME_STATE *state);
void GameAddObject(GAME_STATE *state, GAME_OBJECT *object);
void GameCleanup(GAME_STATE *state, int freeState);
int GameIsNextBottomCellEmpty(GAME_STATE *state, GAME_OBJECT *obj);
void GameInit(UIH_STATE *window, GAME_STATE *state);
void GamePrintField(GAME_STATE *state);
void GameInitializeGameField(GAME_STATE *state);



#endif // _GAME_H
