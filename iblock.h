#ifndef _IBLOCK_H
#define _IBLOCK_H

typedef struct GAME_OBJECT_IBLOCK {
    int f;
} GAME_OBJECT_IBLOCK;

void IBlockConstructor(GAME_STATE *state);
void IBlockDestructor(GAME_STATE *state, GAME_OBJECT *self);
void IBlockMove(GAME_STATE *state, int dir);
void IBlockRotateA(GAME_STATE *state);
void IBlockRotateB(GAME_STATE *state);
void IBlockRotate(GAME_STATE *state);



#endif

