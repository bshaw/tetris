
#include "windows.h"
#include "GL/gl.h"

#include "math.h"

#include "ui.h"
#include "game.h"
#include "mesh.h"
#include "tile.h"
#include "lblock.h"
#include "iblock.h"




GAME_STATE *MakeGameState(UIH_STATE* window) {
    GAME_STATE *state = malloc(sizeof(GAME_STATE));
    *state = (GAME_STATE) {0};
    return state;
}

void GameRender(UIH_STATE *windowState, GAME_STATE *state) {

    float r = fmod(state->dt/100, 255) / 255;
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClearDepth(1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    for(int i = 0; i < GAME_MAX_OBJECTS; i++) {
        GAME_OBJECT *object = state->objects[i];
        if (object!=NULL && object->removed == 0 && object->fnRender != NULL) {
            ObjectRenderFunc fnObjectRender = (ObjectRenderFunc) object->fnRender;
            fnObjectRender(state, state->objects[i]);
        }
    }
    SwapBuffers(windowState->dc);
}

void GameFlushQueue(GAME_STATE *state) {
    for(int i = 0; i < GAME_MAX_OBJECTS; i++) {
        GAME_OBJECT *object = state->objectsQueue[i];
        if (object != NULL) {
            for(int t = 0; t < GAME_MAX_OBJECTS; t++) {
                if (state->objects[t] == NULL) {
                    state->objects[t] = object;
                    state->objects[t]->uuid = state->nextUUID++;
                    //printf("added a thing to the objects list with uuid of %i\n", state->objects[t]->uuid);

                    /*if (object->fnConstructor != NULL) {
                        ObjectConstructorFunc fnObjectConstructor = (ObjectConstructorFunc) object->fnConstructor;
                        fnObjectConstructor(state, object);
                    }*/

                    state->objectsQueue[i] = NULL;
                    break;
                }
            }
        }
    }
    // remove stuff
    for(int z = 0; z < GAME_MAX_OBJECTS; z++) {
        GAME_OBJECT *object = state->objects[z];
        if (object != NULL && object->removed == 1) {
            if (object->fnDestructor != NULL) {
                ObjectDestructorFunc fnObjectDestructor = (ObjectDestructorFunc) object->fnDestructor;
                fnObjectDestructor(state, object);
            }
            free(state->objects[z]);
            state->objects[z] = NULL;
        }
    }
}

int GameShiftTileDown(GAME_STATE *state, int x, int y) {
    GAME_OBJECT *tile = state->field[x][y].object;
    printf("trace 3.1\n");
    if (state->field[x][y+1].object != NULL) {
        printf("trace 3.1.1\n");
        return 0;
    }
    if(tile == NULL) {
        printf("trace 3.1.2\n");
        return 0;
    }
    if (y+1 > GAME_FIELD_HEIGHT - 1) {
        printf("trace 3.1.3\n");
        return 0;
    }
    printf("trace 3.2\n");
    tile->x = state->field[x][y+1].x;
    tile->y = state->field[x][y+1].y;
    printf("trace 3.3\n");
    state->field[x][y].object = NULL;
    state->field[x][y+1].object = tile;
    printf("trace 3.4\n");
    return 1;
}
void GameCleanLines(GAME_STATE *state) {
    int lines = 0;
    int offset = 0;
    for(int y = GAME_FIELD_HEIGHT - 1; y > 0; y--) {
        int count = 0;
        for(int x = 1; x < GAME_FIELD_WIDTH; x++) {
            GAME_OBJECT *tile = state->field[x][y].object;
            if (tile != NULL) {
                count++;
                if (count >= GAME_FIELD_WIDTH - 1) {
                    lines++;
                    for(int x = 1; x < GAME_FIELD_WIDTH; x++) {
                        state->field[x][y].object->removed = 1;
                        state->field[x][y].object = NULL;
                        //printf("ASD??ASD?\n");
                    }
                }
            }
        }
    }
    if (lines > 0) {
        GameSetScore(state, state->score * lines);
        for(int i = 0; i < lines; i++) {
            //for(int y = 0/*GAME_FIELD_HEIGHT - 1*/; y > GAME_FIELD_HEIGHT - 1; y++) {
            //printf("trace 0\n");
            for(int y = GAME_FIELD_HEIGHT - 1; y > 1; y--) {
                //printf("trace 1\n");
                for(int x = 1; x < GAME_FIELD_WIDTH; x++) {
                    //printf("trace 2\n");
                    GAME_OBJECT *tile = state->field[x][y].object;
                    //printf("tile addr = %p\n", tile);
                    if (tile != NULL) {
                        printf("trace 3.0\n");
                        if (!GameShiftTileDown(state, x, y)) {
                                printf("trace 3.5\n");
                            break;
                        }
                        //printf("trace 3.6\n");
                    }
                    //printf("trace 3.7\n");
                }
            }
        }
    }
}

void GameUpdate(UIH_STATE *window, GAME_STATE *state) {

    GameStepActive(state);

    for(int i = 0; i < GAME_MAX_OBJECTS; i++) {
        GAME_OBJECT *object = state->objects[i];

        if (object!=NULL && object->removed == 0 && object->fnStep != NULL) {
            ObjectStepFunc fnObjectStep = (ObjectStepFunc) object->fnStep;
            fnObjectStep(state, state->objects[i]);
        }
    }

   // printf("state->active = %i\n", state->active);

    /*if (state->active != 0) {
        wchar_t gameover[] = L"Game over man, game over!";
        wchar_t winner[] = L"You win, some how.";
        int msg = MessageBoxW(0, state->active == 1 ? gameover : winner, L"Game over!", MB_RETRYCANCEL);
        if (msg == IDRETRY) {
            GameInit(state->window, state);
        }
        else {
            state->window->isRunning = 0;
        }
    }*/

}

void GameAddObject(GAME_STATE *state, GAME_OBJECT *object) {
    if (state->numberOfObjects < GAME_MAX_OBJECTS) {
        for(int i = 0; i < GAME_MAX_OBJECTS; i++ ) {
            if (state->objectsQueue[i] == NULL) {
                state->objectsQueue[i] = object;
                break;
            }
        }
    }
}

void GameCleanup(GAME_STATE *state, int freeState) {
    for(int i = 0; i < GAME_MAX_OBJECTS; i++) {
        free(state->objects[i]);
        free(state->objectsQueue[i]);

        state->objects[i] = NULL;
        state->objectsQueue[i] = NULL;
    }

    for(int x = 0; x < GAME_FIELD_WIDTH; x++) {
        for(int y = 0; y < GAME_FIELD_HEIGHT; y++) {
            //free(state->field[x][y]);
            state->field[x][y].object = NULL;
        }
    }

    if (freeState)
        free(state);
}

void GameInit(UIH_STATE *window, GAME_STATE *state) {

    /*GAME_STATE *game = MakeGameState(window);
    window->datums[0] = game;

    game->view.fov = (45.0f * PI) / 360;
    game->view.znear = 0.01f;
    game->view.zfar = 1000.0f;

    game->view.width = 600;
    game->view.height = 800;
    game->view.aspect = game->view.width / game->view.height;

    game->view.bottom = tan(game->view.fov) * game->view.znear;
    game->view.top = -(game->view.bottom);

    game->view.right = game->view.bottom * game->view.aspect;
    game->view.left = -(game->view.right);

    glViewport(0, 0, game->view.width, game->view.height);

    CreateCubeMesh(game);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glFrustum(game->view.left, game->view.right, game->view.top, game->view.bottom, game->view.znear, game->view.zfar);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();*/

    GameCleanup(state, 0);

    state->window = window;
    state->timeStep = 1000/30;
    state->fallSpeed = 150;
    state->active = 0;
    state->score = 0;
    state->level = 0;

    int time = timeGetTime();
    state->dt = time;
    state->frameTime = time;
    state->lastFallTime = time;
    for(int i = 0; i < GAME_MAX_ACTIVE_TILES; i++)
        state->activeTiles[i].inUse = 0;

    GameInitializeGameField(state);

    GameCreateActiveTile(state);

}

void GameSetLevel(GAME_STATE *state, int level) {
    state->level = level;
    char text[128];
    sprintf(text, "Level: %s", level);
    UIHSetString(state->window->datums[2], text);
}

void GameSetScore(GAME_STATE *state, int score) {
    state->score = score;
    char text[128];
    sprintf(text, "Score: %s", score);
    UIHSetString(state->window->datums[1], text);
}

void GameRotateActiveTiles(GAME_STATE *state) {

    switch(state->activeTileType) {
        case GAME_TILE_TYPE_I: {
            IBlockRotate(state);
            break;
        }
        default: {
            break;
        }
    }
}

void GameActiveBlockMove(GAME_STATE *state, int dir) {
    if (state->activeTileType == GAME_TILE_TYPE_I) {
        //IBlockMove(state, dir);
        //GameMoveActiveTiles(state, dir);
    }
}

void GameStepActive(GAME_STATE *state) {

    if (state->window->keys[VK_SPACE].isActive == 1) {
        state->window->keys[VK_SPACE].isActive = 0;
        GamePrintField(state);
    }
    else if (state->window->keys[VK_Q].isActive == 1) {
        state->window->keys[VK_Q].isActive = 0;
        GameRotateActiveTiles(state);
    }
    else if (state->window->keys[VK_A].isActive == 1) {
        //GameActiveBlockMove(state, GAME_DIRECTION_LEFT);
        GameMoveActiveTiles(state, GAME_DIRECTION_LEFT);
    }
    else if (state->window->keys[VK_D].isActive == 1) {
        //GameActiveBlockMove(state, GAME_DIRECTION_RIGHT);
        GameMoveActiveTiles(state, GAME_DIRECTION_RIGHT);
    }
    if (state->window->keys[VK_S].isActive == 1 || state->dt > state->lastFallTime) {
        state->lastFallTime = state->dt + 1000;
        //GameActiveBlockMove(state, GAME_DIRECTION_DOWN);
        GameMoveActiveTiles(state, GAME_DIRECTION_DOWN);
    }
    else if (state->window->keys[VK_W].isActive == 1) {

    }

}

void GameInitializeGameField(GAME_STATE *state) {
    for(int x = 0; x < GAME_FIELD_WIDTH; x++) {
        for (int y = 0; y < GAME_FIELD_HEIGHT; y++) {
            double dx = FIELD_LEFT + (GAME_FIELD_SQUARE_SIZE * x);
            double dy = FIELD_TOP - (GAME_FIELD_SQUARE_SIZE * y);
            state->field[x][y].x = dx;
            state->field[x][y].y = dy;
            state->field[x][y].object = NULL;
            /*GAME_OBJECT *tile = TileConstructor(state, 1);
            tile->x = dx;
            tile->y = dy;
            GameAddObject(state, tile);*/
        }
    }

    printf("x0, y0 = %f, %f\n", state->field[0][0].x, state->field[0][0].y );
    printf("x%i, y%i = %f, %f\n",GAME_FIELD_WIDTH - 1, GAME_FIELD_HEIGHT - 1,
           state->field[GAME_FIELD_WIDTH - 1][GAME_FIELD_HEIGHT - 1].x,
           state->field[GAME_FIELD_WIDTH - 1][GAME_FIELD_HEIGHT - 1].y );

    /*GAME_OBJECT *tile = TileConstructor(state, 1);
    tile->x = FIELD_LEFT + (GAME_FIELD_SQUARE_SIZE * 1);
    tile->y = FIELD_TOP - (GAME_FIELD_SQUARE_SIZE * 2);
    GameAddObject(state, tile);*/
}

void GameCreateActiveTile(GAME_STATE *state) {

    IBlockConstructor(state);
    printf("%p", state->activeTiles[0]);


}

int GameIsCellEmpty(GAME_STATE *state, int x, int y) {
    if (state->field[x][y].object == NULL) {
        return 1;
    }
    return 0;
}

int GameMoveActivePiecesToCell(GAME_STATE *state, GAME_OBJECT *self, int x, int y, int index) {
    if (x > 0 && x < GAME_FIELD_WIDTH &&
        y > 0 && y < GAME_FIELD_HEIGHT &&
        state->field[x][y].object == NULL && self != NULL) {
        state->field[x][y].object = self;
        state->activeTiles[index].x = x;
        state->activeTiles[index].y = y;
        self->x = state->field[x][y].x;
        self->y = state->field[x][y].y;
        return 0;
    }

    return 1;
}


void GameMoveActiveTiles(GAME_STATE *state, int dir) {

    unsigned int ax1 = state->activeTiles[0].x;
    unsigned int ay1 = state->activeTiles[0].y;

    unsigned int ax2 = state->activeTiles[1].x;
    unsigned int ay2 = state->activeTiles[1].y;

    unsigned int ax3 = state->activeTiles[2].x;
    unsigned int ay3 = state->activeTiles[2].y;

    unsigned int ax4 = state->activeTiles[3].x;
    unsigned int ay4 = state->activeTiles[3].y;

    GAME_OBJECT *activeTile1 = state->field[ax1][ay1].object;
    GAME_OBJECT *activeTile2 = state->field[ax2][ay2].object;
    GAME_OBJECT *activeTile3 = state->field[ax3][ay3].object;
    GAME_OBJECT *activeTile4 = state->field[ax4][ay4].object;

    int collision = 0;
    switch(dir) {
        case GAME_DIRECTION_LEFT: {

            if(!state->activeReady) break;

            if (ax1 > 1 && ax2 > 1 && ax3 > 1 && ax4 > 1) {
                // clear the field of active pieces  so there is no collision with itself
                state->field[ax1][ay1].object = NULL;
                state->field[ax2][ay2].object = NULL;
                state->field[ax3][ay3].object = NULL;
                state->field[ax4][ay4].object = NULL;

                if (GameIsCellEmpty(state, ax1 - 1, ay1) &&
                    GameIsCellEmpty(state, ax2 - 1, ay2) &&
                    GameIsCellEmpty(state, ax3 - 1, ay3) &&
                    GameIsCellEmpty(state, ax4 - 1, ay4)) {

                    GameMoveActivePiecesToCell(state, activeTile1, ax1 - 1, ay1, 0);
                    GameMoveActivePiecesToCell(state, activeTile2, ax2 - 1, ay2, 1);
                    GameMoveActivePiecesToCell(state, activeTile3, ax3 - 1, ay3, 2);
                    GameMoveActivePiecesToCell(state, activeTile4, ax4 - 1, ay4, 3);

                }
                else {

                    state->field[ax1][ay1].object = activeTile1;
                    state->field[ax2][ay2].object = activeTile2;
                    state->field[ax3][ay3].object = activeTile3;
                    state->field[ax4][ay4].object = activeTile4;

                }
            }
            break;
        }
        case GAME_DIRECTION_RIGHT: {

            if(!state->activeReady) break;

            if (!(ay1 < GAME_FIELD_HEIGHT - 2 && ay2 < GAME_FIELD_HEIGHT - 2 &&
                ay3 < GAME_FIELD_HEIGHT - 2 && ay4 < GAME_FIELD_HEIGHT - 2))
                break;

            if (ax1 < GAME_FIELD_WIDTH - 1 && ax2 < GAME_FIELD_WIDTH - 1 &&
                ax3 < GAME_FIELD_WIDTH - 1 && ax4 < GAME_FIELD_WIDTH - 1) {
                state->field[ax1][ay1].object = NULL;
                state->field[ax2][ay2].object = NULL;
                state->field[ax3][ay3].object = NULL;
                state->field[ax4][ay4].object = NULL;
                if (GameIsCellEmpty(state, ax1 + 1, ay1) &&
                    GameIsCellEmpty(state, ax2 + 1, ay2) &&
                    GameIsCellEmpty(state, ax3 + 1, ay3) &&
                    GameIsCellEmpty(state, ax4 + 1, ay4)) {
                    GameMoveActivePiecesToCell(state, activeTile1, ax1 + 1, ay1, 0);
                    GameMoveActivePiecesToCell(state, activeTile2, ax2 + 1, ay2, 1);
                    GameMoveActivePiecesToCell(state, activeTile3, ax3 + 1, ay3, 2);
                    GameMoveActivePiecesToCell(state, activeTile4, ax4 + 1, ay4, 3);
                }
                else {
                    state->field[ax1][ay1].object = activeTile1;
                    state->field[ax2][ay2].object = activeTile2;
                    state->field[ax3][ay3].object = activeTile3;
                    state->field[ax4][ay4].object = activeTile4;
                }
            }
            break;
        }
        case GAME_DIRECTION_DOWN:
        default: {
            if (ay1 < GAME_FIELD_HEIGHT - 1 && ay2 < GAME_FIELD_HEIGHT - 1 &&
                ay3 < GAME_FIELD_HEIGHT - 1 && ay4 < GAME_FIELD_HEIGHT - 1) {
                state->field[ax1][ay1].object = NULL;
                state->field[ax2][ay2].object = NULL;
                state->field[ax3][ay3].object = NULL;
                state->field[ax4][ay4].object = NULL;
                if (GameIsCellEmpty(state, ax1, ay1 + 1) &&
                    GameIsCellEmpty(state, ax2, ay2 + 1) &&
                    GameIsCellEmpty(state, ax3, ay3 + 1) &&
                    GameIsCellEmpty(state, ax4, ay4 + 1)) {
                    GameMoveActivePiecesToCell(state, activeTile1, ax1, ay1 + 1, 0);
                    GameMoveActivePiecesToCell(state, activeTile2, ax2, ay2 + 1, 1);
                    GameMoveActivePiecesToCell(state, activeTile3, ax3, ay3 + 1, 2);
                    GameMoveActivePiecesToCell(state, activeTile4, ax4, ay4 + 1, 3);
                    state->activeReady = 1;
                }
                else {
                    state->field[ax1][ay1].object = activeTile1;
                    state->field[ax2][ay2].object = activeTile2;
                    state->field[ax3][ay3].object = activeTile3;
                    state->field[ax4][ay4].object = activeTile4;
                    collision = 1;
                }

            }
            else {
                collision = 1;
            }
            break;
        }
    }

    if (collision > 0) {

        GameCleanLines(state);
        state->activeReady = 0;

        //
        if ((ay1 <= 1 && ay2 <= 1 &&
                ay3 <= 1  && ay4 <= 1)) {
            state->active = 1;
            return;
        }
        IBlockConstructor(state);
    }
}

int GameMoveActivePiecesToCell2(GAME_STATE *state, int x, int y, int nx, int ny, int index) {
    printf("x = %i, y = %i, nx = %i, ny = %i, index = %i\n", x, y, nx, ny, index);
    if (x < GAME_FIELD_WIDTH - 1 && x > 0 && y < GAME_FIELD_HEIGHT - 1) {
        GAME_OBJECT *obj = state->field[x][y].object;
        if (state->field[nx][ny].object == NULL || state->field[nx][ny].object->isActive == 1) {

            state->field[nx][ny].object = state->field[x][y].object;
            state->field[x][y].object = NULL;

            state->activeTiles[index].x = nx;
            state->activeTiles[index].y = ny;

            state->field[nx][ny].object->x = state->field[nx][ny].x;
            state->field[nx][ny].object->y = state->field[nx][ny].y;

        }
    }
    return 0;
}

void GamePrintField(GAME_STATE *state) {

    printf("++++++++++++++\n");
    for(int y = 0; y < GAME_FIELD_HEIGHT; y++) {
        for(int x = 0; x < GAME_FIELD_WIDTH; x++) {
            printf("%s", state->field[x][y].object == NULL ? "-" : "#");
        }
        printf("\n");
    }
    printf("++++++++++++++\n");

    printf("0: ax = %i, ay = %i\n", state->activeTiles[0].x, state->activeTiles[0].y);
    printf("1: ax = %i, ay = %i\n", state->activeTiles[1].x, state->activeTiles[1].y);
    printf("2: ax = %i, ay = %i\n", state->activeTiles[2].x, state->activeTiles[2].y);
    printf("3: ax = %i, ay = %i\n", state->activeTiles[3].x, state->activeTiles[3].y);

    printf("0: wx = %lf, wy = %lf\n", state->field[state->activeTiles[0].x][state->activeTiles[0].y].x, state->field[state->activeTiles[0].x][state->activeTiles[0].y].y);
    printf("1: wx = %lf, wy = %lf\n", state->field[state->activeTiles[1].x][state->activeTiles[1].y].x, state->field[state->activeTiles[1].x][state->activeTiles[1].y].y);
    printf("2: wx = %lf, wy = %lf\n", state->field[state->activeTiles[2].x][state->activeTiles[2].y].x, state->field[state->activeTiles[2].x][state->activeTiles[2].y].y);
    printf("3: wx = %lf, wy = %lf\n", state->field[state->activeTiles[3].x][state->activeTiles[3].y].x, state->field[state->activeTiles[3].x][state->activeTiles[3].y].y);

    printf("0: ox = %lf, oy = %lf\n", state->field[state->activeTiles[0].x][state->activeTiles[0].y].object->x, state->field[state->activeTiles[0].x][state->activeTiles[0].y].object->y);
    printf("1: ox = %lf, oy = %lf\n", state->field[state->activeTiles[1].x][state->activeTiles[1].y].object->x, state->field[state->activeTiles[1].x][state->activeTiles[1].y].object->y);
    printf("2: ox = %lf, oy = %lf\n", state->field[state->activeTiles[2].x][state->activeTiles[2].y].object->x, state->field[state->activeTiles[2].x][state->activeTiles[2].y].object->y);
    printf("3: ox = %lf, oy = %lf\n", state->field[state->activeTiles[3].x][state->activeTiles[3].y].object->x, state->field[state->activeTiles[3].x][state->activeTiles[3].y].object->y);

}

