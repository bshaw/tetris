
#include "GL/gl.h"

#include "ui.h"
#include "game.h"
#include "tile.h"
#include "mesh.h"
#include "lblock.h"
#include "iblock.h"


//int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow) {
enum MENU {
    GAME_NEW = UIH_MENU_UUID_RANGE,
    GAME_QUIT
};

void onWindowResize(UIH_STATE *state, UIH_CONTROL_RECT *rect) {

    if (state->datums[0] != NULL) {
        GAME_STATE *game = (GAME_STATE*) state->datums[0];

        if (game != NULL) {

            printf("%i %i\n", rect->width, rect->height);

            /*game->view.fov = 80.0;
            game->view.znear = 0.1;
            game->view.zfar = 100.0;*/

            /*game->view.width = rect->width;
            game->view.height = rect->height;
            game->view.aspect = game->view.width / game->view.height;*/
            /*game->view.right = game->view.height * game->view.aspect;
            game->view.left = -(game->view.right);*/

            /*if (rect->width > rect->height)
                glViewport(0, 0, game->view.width, game->view.height);
            else
                glViewport(0, 0, game->view.height * game->view.aspect, game->view.width);*/

            //game->view.fov = (50.0f * PI) / 180;
            //game->view.znear = 0.01f;
            //game->view.zfar = 1000.0f;

            /*game->view.width = rect->width;
            game->view.height = rect->height;
            game->view.aspect = game->view.width / game->view.height;

            game->view.bottom = tan(game->view.fov) * game->view.znear;
            //game->view.bottom = tan(game->view.fov) * game->view.znear;
            //game->view.bottom = cos(game->view.fov) * game->view.znear;
            game->view.top = -(game->view.bottom);

            game->view.right = game->view.bottom * game->view.aspect;
            game->view.left = -(game->view.right);



            glViewport(0, 0, game->view.width, game->view.height);



            glMatrixMode(GL_PROJECTION);
            glLoadIdentity();
            glFrustum(game->view.left, game->view.right, game->view.top, game->view.bottom, game->view.znear, game->view.zfar);
            //glFrustum(-1, 1, -1, 1, 1.0f, 12.0f);
            //glOrtho(game->view.left, game->view.right, game->view.top, game->view.bottom, game->view.znear, game->view.zfar);
            glMatrixMode(GL_MODELVIEW);
            glLoadIdentity();*/







            /*game->view.bottom = tan(game->view.fov / 360 * PI) * game->view.znear;
            game->view.top = -(game->view.bottom);

            glMatrixMode(GL_PROJECTION);
            glLoadIdentity();
            glFrustum(game->view.left, game->view.right, game->view.top, game->view.bottom, game->view.znear, game->view.zfar);
            //glOrtho(game->view.left, game->view.right, game->view.top, game->view.bottom, game->view.znear, game->view.zfar);
            glMatrixMode(GL_MODELVIEW);
            glLoadIdentity();*/

        }

    }
}

void oddwargIsButt(UIH_STATE *state, UIH_CONTROL *control, void *data) {
    MessageBoxW(0, L"Oddwarg is BUTTS", L"butts", 0);
}

int main(int argc, char* args[]) {

    UIH_STATE *window = UIHMakeState();
    if (window != NULL) {
        UIHInit(window);

        int xi = UIHLoadFont(window, "Arial", 30);
        printf("xi = %i\n", xi);


        //window->fnWindowResizeCallback = &onWindowResize;
        UIHCreateWindow(window, "Tetris", 0, 0, 800, 800);

        //UIHAddButton(window, "Oddwarg is butts", 0, 610, 20, 160, 50, &oddwargIsButt, NULL);
        UIH_CONTROL *labelScore = UIHAddLabel(window, "Score: 0", 1, 610, 10, 100, 50);
        UIH_CONTROL *labelLevel = UIHAddLabel(window, "Level: 0", 1, 610, 50, 100, 50);


        HWND hwnd = UIHCreateOwnDCWindow(window, "screen",0, 0, 600, 800, window->hwnd);//, hInstance);




        printf("*****\ngetlasterror = %i\n", GetLastError());

        HMENU child = CreateMenu();
        HMENU parent = CreateMenu();



        AppendMenuW(parent, MF_POPUP, (UINT_PTR) child, L"File");
        AppendMenuW(child, MF_STRING, GAME_NEW, L"&New Game\tCTRL+N");
        AppendMenuW(child, MF_SEPARATOR, 0, NULL);
        AppendMenuW(child, MF_STRING, GAME_QUIT, L"&Quit\tCtrl+Q");

        SetMenu(window->hwnd, parent);


        UIHShowWindow(window, 1);
        //UIHShowWindow(hwnd, 1);
        PIXELFORMATDESCRIPTOR pixelFormat = {0};

        pixelFormat.nSize = sizeof(PIXELFORMATDESCRIPTOR);
        pixelFormat.nVersion = 1;
        pixelFormat.dwFlags = PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER | PFD_DRAW_TO_WINDOW;
        pixelFormat.iPixelType = PFD_TYPE_RGBA;
        pixelFormat.cColorBits = 32;
        pixelFormat.cRedBits = 0;
        pixelFormat.cRedShift = 0;
        pixelFormat.cGreenBits = 0;
        pixelFormat.cGreenShift = 0;
        pixelFormat.cBlueBits = 0;
        pixelFormat.cBlueShift = 0;
        pixelFormat.cAlphaBits = 0;
        pixelFormat.cAlphaShift = 0;
        pixelFormat.cAccumBits = 0;
        pixelFormat.cAccumRedBits = 0;
        pixelFormat.cAccumGreenBits = 0;
        pixelFormat.cAccumBlueBits = 0;
        pixelFormat.cAccumAlphaBits = 0;
        pixelFormat.cDepthBits = 24;
        pixelFormat.cStencilBits = 8;
        pixelFormat.cAuxBuffers = 0;
        pixelFormat.iLayerType = PFD_MAIN_PLANE;
        pixelFormat.bReserved = 0;
        pixelFormat.dwLayerMask = 0;
        pixelFormat.dwVisibleMask = 0;
        pixelFormat.dwDamageMask = 0;

        int format = ChoosePixelFormat(window->dc, &pixelFormat);
        SetPixelFormat(window->dc, format, &pixelFormat);
        HGLRC context = wglCreateContext(window->dc);
        wglMakeCurrent(window->dc, context);

        printf("GL_VERSION = %s\n", glGetString(GL_VERSION));

        //glEnable(GL_SCISSOR_TEST);
        //glScissor(0, 0, 500, 500);

        GAME_STATE *game = MakeGameState(window);
        window->datums[0] = game;
        window->datums[1] = labelScore;
        window->datums[2] = labelLevel;

        game->view.fov = (45.0f * PI) / 360;
        game->view.znear = 0.01f;
        game->view.zfar = 1000.0f;

        game->view.width = 600;
        game->view.height = 800;
        game->view.aspect = game->view.width / game->view.height;

        game->view.bottom = tan(game->view.fov) * game->view.znear;
        game->view.top = -(game->view.bottom);

        game->view.right = game->view.bottom * game->view.aspect;
        game->view.left = -(game->view.right);

        glViewport(0, 0, game->view.width, game->view.height);

        CreateCubeMesh(game);

        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        glFrustum(game->view.left, game->view.right, game->view.top, game->view.bottom, game->view.znear, game->view.zfar);
        //glFrustum(-1, 1, -1, 1, 1.0f, 12.0f);
        //glOrtho(0.0f, game->view.width, game->view.height, 0.0f, 0.0f, 1.0f);
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();

        //CreateCubeMesh(game);

        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LEQUAL);
        glEnable(GL_FLAT);
        glEnable(GL_CULL_FACE);
        //glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);



        /*game->view.aspect = 600/800;
        game->view.fov = 45.0;

        game->view.znear = 0.1;
        game->view.zfar = 100.0;

        game->view.bottom = tan(game->view.fov / 360 * PI) * game->view.znear;
        game->view.top = -(game->view.bottom);

        game->view.right = game->view.height * game->view.aspect;
        game->view.left = -(game->view.left);


        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        glFrustum(game->view.left, game->view.right, game->view.top, game->view.bottom, game->view.znear, game->view.zfar);
        //glFrustum(-width, width, -height, height, mnear, mfar);
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();*/
        //glFrustum(left, right, bottom, top, near, far);

        window->isRunning = 1;


        //int x = 5;
        //int y = 10;
        /*
        for(int x = 0; x < GAME_FIELD_WIDTH; x++) {
            for(int y = 0; y < GAME_FIELD_HEIGHT; y++) {*/
                //GAME_OBJECT *tile1 = TileConstructor(game, 1);
                //tile1->x = FIELD_LEFT + (GAME_FIELD_SQUARE_SIZE * x);
                //tile1->y = FIELD_BOTTOM + (GAME_FIELD_SQUARE_SIZE * y);
                //printf("x[%i] = %lf, y[%i] = %lf\n", x, tile1->x, y, tile1->y);
                //GameAddObject(game, tile1);
            /*}
        }*/
        //tile1->x = ;//-1.35f + (GAME_FIELD_SQUARE_SIZE * 1);
        //GAME_OBJECT *tile2 = TileConstructor(game);
        /*tile2->x = -1.35f + (GAME_FIELD_SQUARE_SIZE * 2);
        GAME_OBJECT *tile3 = TileConstructor(game);
        tile3->x = -1.35f + (GAME_FIELD_SQUARE_SIZE * 3);
        GAME_OBJECT *tile4 = TileConstructor(game);
        tile4->x = -1.35f + (GAME_FIELD_SQUARE_SIZE * 4);*/
        //tile1->y = FIELD_BOTTOM;
        //tile2->y = FIELD_BOTTOM + (GAME_FIELD_SQUARE_SIZE);


        //GameAddObject(game, tile2);
        /*GameAddObject(game, tile3);
        GameAddObject(game, tile4);*/

        GameInit(window, game);

        while(window->isRunning) {
            UIHEventUpdate(window);

            long int now = timeGetTime();
            if (game->dt > game->frameTime + 1000) {
                game->fps = game->frameCount;
                game->frameCount = 0;
                game->frameTime = game->dt;

                char tmpBuffer[100];
                sprintf(tmpBuffer, "Tetris - fps: %i", game->fps);
                wchar_t *title = UIHCharToWide(tmpBuffer);
                SetWindowTextW(window->hwnd, title);
                free(title);

            }

            if (now - game->dt > 1000) {
                game->dt = now;
            }

            GameFlushQueue(game);
            while (game->dt < now) {
                game->dt += game->timeStep;
                GameUpdate(window, game);
            }
            GameRender(window, game);

            game->frameCount++;

        }

        GameCleanup(game, 1);

    }
    else {
        //UIHDisplayError("Feckin' memory didn't allocate yo", __FUNCTION__, 1, "Error");
        return -1;
    }
    return 0;
}
