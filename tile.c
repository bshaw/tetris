#include "GL/gl.h"

#include "ui.h"
#include "game.h"
#include "tile.h"

GAME_OBJECT *TileConstructor(GAME_STATE *state, int isActive) {
    GAME_OBJECT *object = malloc(sizeof(GAME_OBJECT));
    *object = (GAME_OBJECT) {0};

    object->fnConstructor = &TileConstructor;
    object->fnDestructor = &TileDestructor;
    object->fnStep = &TileStep;
    object->fnRender = &TileRender;
    object->extends = malloc(sizeof(GAME_OBJECT_TILE));

    GAME_OBJECT_TILE *selfExt = (GAME_OBJECT_TILE*) object->extends;
    *selfExt = (GAME_OBJECT_TILE) {0};

    selfExt->fallStepTime = state->dt;
    selfExt->moveStepTime = state->dt;


    object->isActive = isActive;
    object->rotation = 0.0f;

    srand((int)state->dt * rand());

    object->xrotation = rand() % 10 > 5 ? 0.0f : 1.0f;
    object->yrotation = rand() % 10 > 5 ? 0.0f : 1.0f;
    object->zrotation = 1.0f;

    object->x = 0;//-2.5f;
    object->y = FIELD_TOP;// - (GAME_FIELD_SQUARE_SIZE * 2);
    object->z = -10.0f;


    object->meshId = 0;
    return object;
}

void TileDestructor(GAME_STATE *state, GAME_OBJECT *self) {
    //printf("henlo from TileDestructor!\n");
    free(self->extends);
}

void TileStep(GAME_STATE *state, GAME_OBJECT *self) {

    GAME_OBJECT_TILE *selfExt = (GAME_OBJECT_TILE*) self->extends;

    if (self->isActive > 0 && state->dt > selfExt->fallStepTime) {
        //selfExt->fallStepTime = state->dt + 1000;
        //GameMoveActiveTiles(state, GAME_DIRECTION_DOWN);

    }
}
    /*if (self->isActive > 0) {

        self->rotation += 3.0f;

        if ((state->window->keys[VK_SPACE].isActive == 1) || (state->dt > selfExt->fallStepTime)) {
            if (GameMoveFromCell(state, self, self->x, self->y - GAME_FIELD_SQUARE_SIZE)) {
                self->y -= GAME_FIELD_SQUARE_SIZE;
            }
            else {

                // game over condition
                if (self->y >= FIELD_TOP - GAME_FIELD_SQUARE_SIZE) {
                    state->active = 0;
                    printf("am i here??\n");
                }

                // create a new tile piece
                else {
                    if (self->isActive == 1) {
                        GameAddObject(state, TileConstructor(state, 1));
                        self->isActive = 2;
                    }
                }

                self->isActive = 0;
                self->rotation = 0.0f;
            }
            selfExt->fallStepTime = state->dt + state->fallSpeed;
        }

        if (self->isActive == 1) {
            if (state->dt > selfExt->moveStepTime) {
                if (state->window->keys[VK_A].isActive) {
                    if (GameMoveFromCell(state, self, self->x - GAME_FIELD_SQUARE_SIZE, self->y)) {
                        self->x -= GAME_FIELD_SQUARE_SIZE;
                    }
                }
                else if (state->window->keys[VK_D].isActive) {
                    if (GameMoveFromCell(state, self, self->x + GAME_FIELD_SQUARE_SIZE, self->y)) {
                        self->x += GAME_FIELD_SQUARE_SIZE;
                    }
                }
                selfExt->moveStepTime = state->dt + 50;
            }
        }
    }
    //}
}*/
    /*UIH_INPUT_STATE *input = state->window->keys;
    if (input[VK_A].isActive && selfExt->isActive) {
        if (self->y > -1.5f + GAME_FIELD_SQUARE_SIZE) {
            self->y -= GAME_FIELD_SQUARE_SIZE;
        }
        else {
            selfExt->isActive = 0;
        }
    }
    if (input[VK_SPACE].isActive) {
        selfExt->isActive = 0;*/

        /*GAME_OBJECT *tile1 = TileConstructor(state);
        tile1->x = -1.35f + (GAME_FIELD_SQUARE_SIZE * 1);
        GAME_OBJECT *tile2 = TileConstructor(state);
        tile2->x = -1.35f + (GAME_FIELD_SQUARE_SIZE * 2);
        GAME_OBJECT *tile3 = TileConstructor(state);
        tile3->x = -1.35f + (GAME_FIELD_SQUARE_SIZE * 3);
        GAME_OBJECT *tile4 = TileConstructor(state);
        tile4->x = -1.35f + (GAME_FIELD_SQUARE_SIZE * 4);

        GameAddObject(state, tile1);
        GameAddObject(state, tile2);
        GameAddObject(state, tile3);
        GameAddObject(state, tile4);*/

    /*if (selfExt->isActive == 1 && state->dt > selfExt->moveStepTime) {
        selfExt->moveStepTime = state->dt + 100;

        if (self->y > -1.5f) {
            self->y -= 0.25f;
        }
        else {
            selfExt->isActive = 0;
        }

    }*/
    //}
    /*if (state->dt > self->canMoveTime ) {
        if (self->y > -1.5) {
            self->y -= 0.25f;
        }
        else {
            self->rotation = 0.0f;
        }
        self->canMoveTime = state->dt + 100;
    }
}*/

void TileRender(GAME_STATE *state, GAME_OBJECT *self) {
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glTranslatef(self->x, self->y, self->z);
    glRotatef(self->rotation, self->xrotation, self->yrotation, self->zrotation);
    glScalef(0.2f, 0.2f, 0.2f);

    glCallList(state->mesh[self->meshId]);
}

    /*double x = 0.0f;
    double y = 0.0f;

    int type = 0;
    double b = 1.0f; // y offset
    double c = 0.51f; // x offset
    double d = 0.25f;
    if (type == 0) {

        // L
        for (int a = 0;a<4;a++) {
            if (a == 3) {
                b=2.0f;
                c=1.51f;
            }
            glMatrixMode(GL_MODELVIEW);
            glLoadIdentity();

            glTranslatef(x + 0.51f * (a-c), y + 0.51f * b, -2.5f);
            glRotatef(self->t, 1.0f, 1.0f, 0.0f);
            glScalef(0.25f, 0.25f, 0.25f);

            glCallList(state->mesh[self->meshId]);

        }

        y = -0.75f;
        x = -0.25f;

        b = 0.0f;
        c = 0.0f;
        d = 0.25f;

        // Square
        for (int a = 0;a<4;a++) {
            if (a>1) {
                b = 1.0f;
                d = 2.25f;
            }
            else {
                b = 0.0f;
                d = 0.25f;
            }
            glMatrixMode(GL_MODELVIEW);
            glLoadIdentity();

            glTranslatef(x + 0.51f * (a - d), y + 0.51f * b, -2.5f);
            glRotatef(self->t, 1.0f, 1.0f, 0.0f);
            glScalef(0.25f, 0.25f, 0.25f);

            glCallList(state->mesh[self->meshId]);

        }

        y = 1.54f;
        x = -0.64f;

        b = 0.0f;
        c = 0.0f;
        d = 0.25f;

        // Square
        for (int a = 0;a<4;a++) {

            glMatrixMode(GL_MODELVIEW);
            glLoadIdentity();

            glTranslatef(x + 0.51f * (a - d), y + 0.51f * b, -2.5f);
            glRotatef(self->t, 1.0f, 1.0f, 0.0f);
            glScalef(0.25f, 0.25f, 0.25f);

            glCallList(state->mesh[self->meshId]);

        }

    }*/
    //}
   // }
//}

//#endif // _TILE_C
